/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javascheduling;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Comparator;
import java.util.List;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.util.Callback;
import javascheduling.DAL.JavaSchedulerDatabase;
import javascheduling.DAL.models.*;

/**
 * @author michael.evanson
 */
public class MainController implements Initializable {

    private boolean isMonthView = false;
    private User user;

    private final ObservableList<Appointment> appointments = FXCollections.observableArrayList();

    @FXML
    private Label label;

    @FXML
    private Button switchViewButton;
    @FXML
    private Button addAppointmentButton;
    @FXML
    private Button appointmentTypeReportButton;
    @FXML
    private Button consultantScheduleButton;
    @FXML
    private Button numberOfCustomerReportButton;

    @FXML
    private TableView<Appointment> tableView;
    @FXML
    private TableColumn<Appointment, String> customerName;
    @FXML
    private TableColumn<Appointment, String> appointmentDate;
    @FXML
    private TableColumn<Appointment, String> appointmentTime;
    @FXML
    private TableColumn<Appointment, String> appointmentActions;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        addAppointmentButton.setOnAction(this::addAppointmentHandler);
        appointmentTypeReportButton.setOnAction(this::appointmentTypeReportHandler);
        numberOfCustomerReportButton.setOnAction(this::activeCustomersReportHandler);
        consultantScheduleButton.setOnAction(this::consultantScheduleReportHandler);
        tableView.setItems(appointments);
        setupTableCellFactories();
    }

    private void fetchAppointments() {
        try {
            LocalDateTime startRange = LocalDateTime.now().toLocalDate().atStartOfDay();
            LocalDateTime endRange = isMonthView ? startRange.plusMonths(1) : startRange.plusDays(7);
            List<Appointment> fetchedAppointments = JavaSchedulerDatabase.Repository.getAllAppointmentsByDateRangeAndUser(startRange, endRange, user.getId());
            appointments.setAll(fetchedAppointments);
            if (appointments.size() > 0) {
                LocalDateTime currentTime = LocalDateTime.now();
                FilteredList<Appointment> futureAppointments = appointments.filtered(appointment -> appointment.getStart().isAfter(currentTime)); // Use a lambda here for a more expressive predicate
                if (futureAppointments.size() > 0) {
                    Appointment nextAppointment = futureAppointments.sorted(Comparator.comparing(Appointment::getStart)).get(0);
                    if (nextAppointment.getStart().isBefore(currentTime.plusMinutes(15))) {
                        Alert upcomingAppointment = new Alert(Alert.AlertType.INFORMATION, "You have an appointment in the next 15 minutes", ButtonType.OK);
                        upcomingAppointment.show();
                    }
                }
            }
        } catch (IOException | SQLException e) {
            e.printStackTrace();
        }
    }

    private void setupTableCellFactories() {
        Callback<TableColumn<Appointment, String>, TableCell<Appointment, String>> appointmentDateFactory = new Callback<TableColumn<Appointment, String>, TableCell<Appointment, String>>() {
            @Override
            public TableCell<Appointment, String> call(TableColumn<Appointment, String> param) {
                final TableCell<Appointment, String> cell = new TableCell<Appointment, String>() {
                    @Override
                    public void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                            setText(null);
                        } else {
                            Appointment appointment = getTableView().getItems().get(getIndex());
                            setText(appointment.getStart().toLocalDate().toString());
                        }
                    }
                };
                return cell;
            }
        };

        Callback<TableColumn<Appointment, String>, TableCell<Appointment, String>> appointmentTimeFactory = new Callback<TableColumn<Appointment, String>, TableCell<Appointment, String>>() {
            @Override
            public TableCell<Appointment, String> call(TableColumn<Appointment, String> param) {
                final TableCell<Appointment, String> cell = new TableCell<Appointment, String>() {
                    @Override
                    public void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                            setText(null);
                        } else {
                            Appointment appointment = getTableView().getItems().get(getIndex());
                            LocalTime startTime = appointment.getStart().toLocalTime();
                            setText(startTime.getHour() + ":" + (startTime.getMinute() < 10 ? "0" : "") + startTime.getMinute());
                        }
                    }
                };
                return cell;
            }
        };

        Callback<TableColumn<Appointment, String>, TableCell<Appointment, String>> customerNameFactory = new Callback<TableColumn<Appointment, String>, TableCell<Appointment, String>>() {
            @Override
            public TableCell<Appointment, String> call(TableColumn<Appointment, String> param) {
                final TableCell<Appointment, String> cell = new TableCell<Appointment, String>() {
                    @Override
                    public void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                            setText(null);
                        } else {
                            Appointment appointment = getTableView().getItems().get(getIndex());
                            try {
                                Customer customer = appointment.getCustomer();
                                setText(customer.getCustomerName());
                            } catch (IOException | SQLException e) {
                                e.printStackTrace();
                                setText("Unknown");
                            }
                        }
                    }
                };
                return cell;
            }
        };

        Callback<TableColumn<Appointment, String>, TableCell<Appointment, String>> appointmentActionsFactory = new Callback<TableColumn<Appointment, String>, TableCell<Appointment, String>>() {
            @Override
            public TableCell<Appointment, String> call(TableColumn<Appointment, String> param) {
                final TableCell<Appointment, String> cell = new TableCell<Appointment, String>() {
                    final Button editButton = new Button("Edit");
                    final Button deleteButton = new Button("Delete");
                    final HBox container = new HBox(editButton, deleteButton);

                    @Override
                    public void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                            setText(null);
                        } else {
                            deleteButton.setOnAction(event -> { // Usage of a lambda here to make the action less buried in verbose code
                                Appointment appointment = getTableView().getItems().get(getIndex());
                                try {
                                    JavaSchedulerDatabase.Repository.deleteAppointmentById(appointment.getId());
                                    fetchAppointments();
                                } catch (SQLException | IOException e) {
                                    e.printStackTrace();
                                    Alert failure = new Alert(Alert.AlertType.ERROR, "Failed to delete this appointment", ButtonType.CLOSE);
                                    failure.show();
                                }
                            });
                            editButton.setOnAction(event -> { // Usage of a lambda here to make the action less buried in verbose code
                                try {
                                    Appointment appointment = getTableView().getItems().get(getIndex());
                                    Customer customer = appointment.getCustomer();
                                    JavaScheduling.getInstance().launchAppointmentController(customer, appointment);
                                } catch (IOException | SQLException e) {
                                    e.printStackTrace();
                                    Alert failure = new Alert(Alert.AlertType.ERROR, "Failed to edit this appointment", ButtonType.CLOSE);
                                    failure.show();
                                }
                            });
                            setGraphic(container);
                        }
                    }
                };
                return cell;
            }
        };
        customerName.setCellFactory(customerNameFactory);
        appointmentDate.setCellFactory(appointmentDateFactory);
        appointmentTime.setCellFactory(appointmentTimeFactory);
        appointmentActions.setCellFactory(appointmentActionsFactory);
    }

    public void setUser(User user) {
        this.user = user;
        fetchAppointments();
    }

    @FXML
    private void handleSwitchView(ActionEvent event) {
        if (isMonthView) {
            label.setText("Week View");
            switchViewButton.setText("Switch to Month View");
        } else {
            label.setText("Month View");
            switchViewButton.setText("Switch to Week View");
        }
        isMonthView = !isMonthView;
        fetchAppointments();
    }

    private void addAppointmentHandler(ActionEvent event) {
        JavaScheduling.getInstance().launchAppointmentController(null, null);
    }

    private void appointmentTypeReportHandler(ActionEvent actionEvent) {
        LocalDateTime currentTime = LocalDateTime.now();
        try {
            List<AppointmentTypeReport> reports = JavaSchedulerDatabase.Repository.getCountOfAppointmentTypesEachMonth(currentTime.getYear(), 0);
            StringBuilder sb = new StringBuilder();
            sb.append("Displaying the number of appointment types for each month in the year ")
                    .append(currentTime.getYear())
                    .append("\r");
            for (AppointmentTypeReport report : reports) {
                sb.append(report.getMonthName())
                        .append("\t")
                        .append(report.getCountOfAppointmentTypes())
                        .append("\r");
            }
            Alert alert = new Alert(Alert.AlertType.INFORMATION, sb.toString(), ButtonType.CLOSE);
            alert.show();
        } catch (SQLException | IOException e) {
            e.printStackTrace();
        }
    }

    private void activeCustomersReportHandler(ActionEvent actionEvent) {
        try {
            int customerCount = JavaSchedulerDatabase.Repository.getCountOfActiveCustomers();
            String correctPluralizationForCount = customerCount == 1 ? " customer" : " customers";
            String correctVerbFormForPluralization = customerCount == 1 ? " is " : " are ";
            Alert alert = new Alert(Alert.AlertType.INFORMATION, "There" + correctVerbFormForPluralization + customerCount + correctPluralizationForCount + " currently active", ButtonType.CLOSE);
            alert.show();
        } catch (SQLException | IOException e) {
            e.printStackTrace();
        }
    }

    private void consultantScheduleReportHandler(ActionEvent actionEvent) {
        try {
            List<ConsultantSchedule> schedules = JavaSchedulerDatabase.Repository.getConsultantSchedules();
            StringBuilder sb = new StringBuilder();
            sb.append("Displaying the schedules for all consultants:\n");
            for (ConsultantSchedule schedule : schedules) {
                sb.append(schedule.getConsultantName())
                        .append(":\n");
                for (String summary : schedule.getAppointments()) {
                    sb.append("\t")
                            .append(summary)
                            .append("\n");
                }
            }
            Alert alert = new Alert(Alert.AlertType.INFORMATION, sb.toString(), ButtonType.CLOSE);
            alert.show();
        } catch (SQLException | IOException e) {
            e.printStackTrace();
        }
    }
}
