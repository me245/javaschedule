/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javascheduling;

import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javascheduling.DAL.models.Appointment;
import javascheduling.DAL.models.Customer;
import javascheduling.DAL.models.User;

/**
 *
 * @author michael.evanson
 */
public class JavaScheduling extends Application {

    private User user;
    private Stage stage;
    private static JavaScheduling instance;
    
    @Override
    public void start(Stage stage) throws Exception {
        this.stage = stage;
        Locale currentLocale = Locale.getDefault();
        ResourceBundle bundle = ResourceBundle.getBundle("resources/UIResources", currentLocale);
        Parent root = FXMLLoader.load(getClass().getResource("/javascheduling/Login.fxml"), bundle);

        Scene scene = new Scene(root);
        instance = this;
        stage.setScene(scene);
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    public static JavaScheduling getInstance() {
        return instance;
    }

    public void launchMain(User user) {
        this.user = user;
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/javascheduling/Main.fxml"));
        Parent root;
        try {
            root = (Parent)loader.load();
            MainController controller = (MainController)loader.getController();
            controller.setUser(user);
            this.stage.setScene(new Scene(root));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void launchAppointmentController(Customer customer, Appointment appointment) {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/javascheduling/Appointment.fxml"));
        Parent root;
        try {
            root = (Parent) loader.load();
            AppointmentController controller = (AppointmentController) loader.getController();
            controller.setUser(user);
            if (customer != null) {
                controller.setSelectedCustomer(customer);
            }
            if (appointment != null) {
                controller.setAppointment(appointment);
            }
            this.stage.setScene(new Scene(root));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void launchCustomerController(Customer customer) {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/javascheduling/Customer.fxml"));
        Parent root;
        try {
            root = (Parent) loader.load();
            CustomerController controller = (CustomerController) loader.getController();
            if (customer != null) {
                controller.setCustomer(customer);
            }
            controller.setUser(user);
            this.stage.setScene(new Scene(root));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
