package javascheduling;

import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javascheduling.DAL.JavaSchedulerDatabase;
import javascheduling.DAL.models.*;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class CustomerController implements Initializable {
    private User user;
    private Country selectedCountry;
    private City selectedCity;
    private boolean isNewCustomer = true;
    private int idForCustomerAddress = -1;
    private int idForSelectedCity = -1;
    private int existingCustomerId = -1;
    private LocalDateTime existingCustomerCreateTime = null;
    private String existingCustomerCreateBy = null;

    private ObservableList<Country> countries = FXCollections.observableArrayList();
    private ObservableList<City> cities = FXCollections.observableArrayList();

    @FXML
    private TextField customerNameTextField;
    @FXML
    private TextField addressTextField;
    @FXML
    private TextField address2TextField;
    @FXML
    private TextField phoneNumberTextField;
    @FXML
    private TextField postalCodeTextField;
    @FXML
    private TextField countryNameTextField;
    @FXML
    private TextField cityNameTextField;

    @FXML private Button addCountryButton;
    @FXML private Button addCityButton;
    @FXML private Button saveButton;
    @FXML private Button cancelButton;

    @FXML private ChoiceBox<ChoiceBoxItemMap> countryChoiceBox;
    @FXML private ChoiceBox<ChoiceBoxItemMap> cityChoiceBox;

    private BooleanBinding countryNameInvalid;
    private BooleanBinding cityNameInvalid;
    private BooleanBinding formInvalid;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        idForCustomerAddress = -1;
        idForSelectedCity = -1;
        countryNameInvalid = Bindings.createBooleanBinding(() -> countryNameTextField.getText().length() <= 0, countryNameTextField.textProperty()); // usage of lambda here to simplify the readability of the code
        cityNameInvalid = Bindings.createBooleanBinding(() -> cityNameTextField.getText().length() <= 0, cityNameTextField.textProperty()); // usage of lambda here to keep consistency in approach
        formInvalid = Bindings.createBooleanBinding(this::isFormInvalid,
                customerNameTextField.textProperty(),
                addressTextField.textProperty(),
                phoneNumberTextField.textProperty(),
                postalCodeTextField.textProperty(),
                countryChoiceBox.valueProperty(),
                cityChoiceBox.valueProperty());

        cityChoiceBox.disableProperty().bind(countryChoiceBox.valueProperty().isNull());
        addCityButton.disableProperty().bind(cityNameInvalid);
        addCountryButton.disableProperty().bind(countryNameInvalid);
        saveButton.disableProperty().bind(formInvalid);

        addCountryButton.setOnAction(this::addCountryButtonHandler);
        addCityButton.setOnAction(this::addCityButtonHandler);
        cancelButton.setOnAction(this::cancelButtonHandler);
        saveButton.setOnAction(this::saveButtonHandler);

        countries.addListener(new CountryListChangeListener());
        cities.addListener(new CityListChangeListener());

        countryChoiceBox.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> { // Use of lambda here to make the listener simpler to read and implement
            if (newValue != null) {
                selectedCountry = countries.filtered(country -> country.getId() == newValue.getKey()).get(0); // use of lambda here to make a more expressive predicate
            } else {
                selectedCountry = null;
            }
            if (selectedCountry != null) {
                EventHandler<ActionEvent> action = cityChoiceBox.getOnAction();
                cityChoiceBox.setOnAction(null);
                cityChoiceBox.getSelectionModel().clearSelection();
                populateCityBox(null, selectedCountry.getId());
                cityChoiceBox.setOnAction(action);
                if (idForSelectedCity > 0) {
                    cityChoiceBox.getSelectionModel().select(cityChoiceBox.getItems().filtered(item -> item.getKey() == idForSelectedCity).get(0)); // use of lambda here to make a more expressive predicate
                }
            }
        });

        cityChoiceBox.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> { // use of lambda to simplify the readability and implementation for the selection listener
            if (newValue != null) {
                if (selectedCity != null && newValue.getKey() != selectedCity.getId()) {
                    selectedCity = cities.filtered(city -> city.getId() == newValue.getKey()).get(0); // use of lambda to simplify predicate
                } else if (selectedCity == null) {
                    selectedCity = cities.filtered(city -> city.getId() == newValue.getKey()).get(0); // use of lambda to simplify predicate
                }
            } else {
                selectedCity = null;
                idForSelectedCity = -1;
            }
        });

        populateCountryBox(null);
    }

    private void populateCityBox(City cityToAdd, int countryId) {
        if (cityToAdd == null) {
            try {
                cities.setAll(JavaSchedulerDatabase.Repository.getAllCitiesFromCountry(countryId));
            } catch (SQLException | IOException e) {
                e.printStackTrace();
            }
        } else {
            cities.add(cityToAdd);
        }
    }

    private void populateCountryBox(Country countryToAdd) {
        if (countryToAdd == null) {
            try {
                countries.setAll(JavaSchedulerDatabase.Repository.getAllCountries());
            } catch (SQLException | IOException e) {
                e.printStackTrace();
            }
        } else {
            countries.add(countryToAdd);
        }
    }


    private boolean isFormInvalid() {
        return customerNameTextField.getText().length() <= 0 &&
                addressTextField.getText().length() <= 0 &&
                phoneNumberTextField.getText().length() <= 0 &&
                postalCodeTextField.getText().length() <= 0 &&
                countryChoiceBox.valueProperty().isNull().isValid() &&
                cityChoiceBox.valueProperty().isNull().isValid();
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setCustomer(Customer customer) {
        isNewCustomer = false;
        existingCustomerId = customer.getId();
        existingCustomerCreateTime = customer.getCreateDate();
        existingCustomerCreateBy = customer.getCreatedBy();
        customerNameTextField.setText(customer.getCustomerName());
        idForCustomerAddress = customer.getAddressId();
        setAddress(idForCustomerAddress);
    }

    private void setAddress(int addressId) {
        Address address = null;
        int countryId;
        try {
            address = JavaSchedulerDatabase.Repository.getAddressById(addressId);
            if (address != null) {
                idForSelectedCity = address.getCityId();
                countryId = JavaSchedulerDatabase.Repository.getCityById(idForSelectedCity).getCountryId();
                countryChoiceBox.getSelectionModel().select(countryChoiceBox.getItems().filtered(item -> item.getKey() == countryId).get(0)); // use of lambda to simplify predicate
                addressTextField.setText(address.getAddress());
                address2TextField.setText(address.getAddress2());
                phoneNumberTextField.setText(address.getPhone());
                postalCodeTextField.setText(address.getPostalCode());
            }
        } catch (SQLException | IOException e) {
            e.printStackTrace();
        }
    }

    private void addCountryButtonHandler(ActionEvent event) {
        Country country = new Country();
        country.setCreationInformation(user);
        country.setUpdateInformation(user);
        country.setCountry(countryNameTextField.getText());
        int countryId = -1;
        try {
            countryId = JavaSchedulerDatabase.Repository.updateOrInsertCountry(country);
        } catch (SQLException | IOException e) {
            e.printStackTrace();
        }
        if (countryId > -1) {
            idForSelectedCity = -1;
            country.setId(countryId);
            populateCountryBox(country);
            countryChoiceBox.getSelectionModel().selectLast();
        }
    }
    private void addCityButtonHandler(ActionEvent event) {
        City city = new City();
        city.setCreationInformation(user);
        city.setUpdateInformation(user);
        city.setCountryId(selectedCountry.getId());
        city.setCity(cityNameTextField.getText());
        int cityId = -1;
        try {
            cityId = JavaSchedulerDatabase.Repository.updateOrInsertCity(city);
        } catch (SQLException | IOException e) {
            e.printStackTrace();
        }
        if (cityId > -1) {
            city.setId(cityId);
            populateCityBox(city, selectedCountry.getId());
            cityChoiceBox.getSelectionModel().selectLast();
        }
    }

    private void cancelButtonHandler(ActionEvent event) {
        JavaScheduling.getInstance().launchAppointmentController(null, null);
    }

    private void saveButtonHandler(ActionEvent event) {
        Customer customerToAdd = saveCustomerFields();
        int customerId;
        try {
            customerId = JavaSchedulerDatabase.Repository.updateOrInsertCustomer(customerToAdd);
            if (customerId > 0) {
                customerToAdd.setId(customerId);
            }
            isNewCustomer = true;
            idForCustomerAddress = -1;
            idForSelectedCity = -1;
            existingCustomerId = -1;
            existingCustomerCreateTime = null;
            existingCustomerCreateBy = null;
            JavaScheduling.getInstance().launchAppointmentController(customerToAdd, null);
        } catch (SQLException | IOException e) {
            e.printStackTrace();
        }
    }

    private Customer saveCustomerFields() {
        Customer customer = new Customer();
        customer.setUpdateInformation(user);
        customer.setCustomerName(customerNameTextField.getText());
        customer.setAddressId(insertAddress().getId());
        customer.setActive(true);
        if (isNewCustomer) {
            customer.setCreationInformation(user);
        } else {
            customer.setId(existingCustomerId);
            customer.setCreateDate(existingCustomerCreateTime);
            customer.setCreatedBy(existingCustomerCreateBy);
        }
        return customer;
    }

    private Address insertAddress() {
        Address address = new Address();
        if (idForCustomerAddress > 0) {
            try {
                address = JavaSchedulerDatabase.Repository.getAddressById(idForCustomerAddress);
            } catch (SQLException | IOException e) {
                e.printStackTrace();
            }
        } else {
            address.setCreationInformation(user);
        }
        address.setUpdateInformation(user);
        address.setAddress(addressTextField.getText());
        address.setAddress2(address2TextField.getText());
        address.setPhone(phoneNumberTextField.getText());
        address.setPostalCode(postalCodeTextField.getText());
        address.setCity(selectedCity);
        address.setCityId(selectedCity.getId());
        try {
            int idResult = JavaSchedulerDatabase.Repository.updateOrInsertAddress(address);
            if (idResult > 0) {
                idForCustomerAddress = idResult;
            }
            address.setId(idForCustomerAddress);
        } catch (SQLException | IOException e) {
            e.printStackTrace();
        }
        return address;
    }

    private class CountryListChangeListener implements ListChangeListener<Country> {

        @Override
        public void onChanged(Change<? extends Country> c) {
            List<ChoiceBoxItemMap> choices = new ArrayList<>();
            countries.forEach(country -> choices.add(new ChoiceBoxItemMap(country.getId(), country.getCountry()))); // use of lambda to simplify foreach loop action
            countryChoiceBox.setItems(FXCollections.observableList(choices));
        }
    }

    private class CityListChangeListener implements  ListChangeListener<City> {

        @Override
        public void onChanged(Change<? extends City> c) {
            List<ChoiceBoxItemMap> choices = new ArrayList<>();
            cities.forEach(city -> choices.add(new ChoiceBoxItemMap(city.getId(), city.getCity()))); // use of lambda to simplify foreach loop action
            cityChoiceBox.setItems(FXCollections.observableList(choices));
        }
    }
}
