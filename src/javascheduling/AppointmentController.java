package javascheduling;

import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.util.Callback;
import javascheduling.DAL.JavaSchedulerDatabase;
import javascheduling.DAL.models.Appointment;
import javascheduling.DAL.models.Customer;
import javascheduling.DAL.models.User;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class AppointmentController implements Initializable {
    private User user;
    private Customer selectedCustomer;
    private Appointment selectedAppointment;
    private boolean isEdit = false;

    private ObservableList<Customer> customers = FXCollections.observableArrayList();

    @FXML
    ChoiceBox<ChoiceBoxItemMap> appointmentTimeChoiceBox;
    @FXML
    ChoiceBox<ChoiceBoxItemMap> appointmentDurationChoiceBox;
    @FXML
    ChoiceBox<ChoiceBoxItemMap> customerChoiceBox;
    @FXML
    Button addNewCustomerButton;
    @FXML
    Button submitButton;
    @FXML
    Button cancelButton;
    @FXML
    Button deleteButton;
    @FXML
    Button editCustomerButton;
    @FXML
    DatePicker appointmentDatePicker;
    @FXML
    TextField appointmentTypeTextField;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        cancelButton.setOnAction(this::cancelHandler);
        setupCustomerManagement();
        populateDurationChoiceBox();
        populateTimeChoiceBox();
        prepareDatePicker();
        setupSubmitButton();
    }

    public void setAppointment(Appointment appointment) {
        if (appointment != null) {
            isEdit = true;
            selectedAppointment = appointment;
            appointmentTypeTextField.setText(appointment.getType());
            appointmentDatePicker.setValue(appointment.getStart().toLocalDate());
            String startHour = appointment.getStart().getHour() + "";
            String startMinute = appointment.getStart().getMinute() < 10 ? "0" + appointment.getStart().getMinute() : appointment.getStart().getMinute() + "";
            String formattedStartTime = startHour + ":" + startMinute;
            long duration = appointment.getStart().until(appointment.getEnd(), ChronoUnit.MINUTES);
            ChoiceBoxItemMap durationChoiceRef = appointmentDurationChoiceBox.getItems().filtered(durationChoice -> durationChoice.getKey() == duration).get(0); // use of lambda to simplify predicate
            appointmentDurationChoiceBox.getSelectionModel().select(durationChoiceRef);
            try {
                ChoiceBoxItemMap appointmentTimeChoiceRef = appointmentTimeChoiceBox.getItems().filtered(time -> time.toString().equalsIgnoreCase(formattedStartTime)).get(0); // use of lambda to simplify predicate
                appointmentTimeChoiceBox.getSelectionModel().select(appointmentTimeChoiceRef);
            } catch (IndexOutOfBoundsException e) {
                Alert wrongTimezoneLikely = new Alert(Alert.AlertType.WARNING, "This appointment occurs out of business hours and was likely set in another timezone. The time will need to be set again to update", ButtonType.OK);
                wrongTimezoneLikely.show();
            }
            ChoiceBoxItemMap customerChoiceRef = customerChoiceBox.getItems().filtered(customer -> customer.getKey() == appointment.getCustomerId()).get(0); // use of lambda to simplify predicate
            customerChoiceBox.getSelectionModel().select(customerChoiceRef);
        }
    }

    private void setupSubmitButton() {
        BooleanBinding customerNotSelected = customerChoiceBox.getSelectionModel().selectedItemProperty().isNull();
        BooleanBinding timeNotSelected = appointmentTimeChoiceBox.getSelectionModel().selectedItemProperty().isNull();
        BooleanBinding durationNotSelected = appointmentDurationChoiceBox.getSelectionModel().selectedItemProperty().isNull();
        BooleanBinding dayNotSelected = appointmentDatePicker.valueProperty().isNull();
        BooleanBinding typeNotFilledOut = appointmentTypeTextField.textProperty().isEmpty();
        submitButton.disableProperty().bind(customerNotSelected.or(timeNotSelected).or(durationNotSelected).or(dayNotSelected).or(typeNotFilledOut));
        submitButton.setOnAction(this::submitHandler);
    }

    private void submitHandler(ActionEvent actionEvent) {
        LocalDateTime appointmentStart = appointmentDatePicker.getValue().atStartOfDay();
        ChoiceBoxItemMap selectedStart = appointmentTimeChoiceBox.getValue();
        int startHour = Integer.parseInt(selectedStart.toString().substring(0, 1), 10);
        int startMinutes = Integer.parseInt(selectedStart.toString().substring(2), 10);
        appointmentStart = appointmentStart.plusHours(startHour).plusMinutes(startMinutes);
        LocalDateTime appointmentEnd = appointmentStart.plusMinutes(appointmentDurationChoiceBox.getValue().getKey());
        Appointment appointment = isEdit ? selectedAppointment : new Appointment();
        appointment.setUserId(user.getId());
        appointment.setCustomerId(selectedCustomer.getId());
        appointment.setType(appointmentTypeTextField.getText());
        appointment.setStart(appointmentStart);
        appointment.setEnd(appointmentEnd);
        appointment.setCreationInformation(user);
        appointment.setUpdateInformation(user);
        appointment.setTitle("");
        appointment.setDescription("");
        appointment.setLocation("");
        appointment.setContact("");
        appointment.setUrl("");
        try {
            int insertedAppointmentIdOrAffectedRows = JavaSchedulerDatabase.Repository.updateOrInsertAppointment(appointment);
            if (insertedAppointmentIdOrAffectedRows <= 0) {
                Alert appointmentOverlapAlert = new Alert(Alert.AlertType.WARNING, "This appointment overlaps with another appointment. Please check the schedule and try again", ButtonType.OK);
                appointmentOverlapAlert.show();
            } else {
                JavaScheduling.getInstance().launchMain(user);
            }
        } catch (SQLException | IOException e) {
            e.printStackTrace();
        }
    }

    private void setupCustomerManagement() {
        addNewCustomerButton.setOnAction((this::addNewCustomerHandler));
        deleteButton.setOnAction(this::handleDeleteCustomer);
        editCustomerButton.setOnAction(this::handleEditCustomer);

        customers.addListener(new CustomerListChangeListener());

        customerChoiceBox.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> { // use of lambda to simplify readability for selection listener
            boolean newValueSelected = newValue != null;
            if (newValueSelected) {
                if (selectedCustomer != null && oldValue != null && newValue.getKey() != oldValue.getKey()) {
                    selectedCustomer = customers.filtered(customer -> customer.getId() == newValue.getKey()).get(0); // use of lambda to simplify predicate
                } else if (selectedCustomer == null || oldValue == null) {
                    selectedCustomer = customers.filtered(customer -> customer.getId() == newValue.getKey()).get(0); // use of lambda to simplify predicate
                }
            } else {
                selectedCustomer = null;
            }
            if (selectedCustomer != null || !newValueSelected) {
                handleCustomerControlsVisibility(newValueSelected);
            }
        });

        populateCustomersChoiceBox();
    }

    private void prepareDatePicker() {
        Callback<DatePicker, DateCell> disablePastDays = new Callback<DatePicker, DateCell>() {
            @Override
            public DateCell call(DatePicker param) {
                return new DateCell() {
                    @Override
                    public void updateItem(LocalDate item, boolean empty) {
                        super.updateItem(item, empty);
                        LocalDate today = LocalDate.now();
                        if (item.compareTo(today) < 0) {
                            setDisable(true);
                            setStyle("-fx-background-color: #ffc0cb;");
                        }
                    }
                };
            }
        };

        appointmentDatePicker.setDayCellFactory(disablePastDays);
    }


    private void handleEditCustomer(ActionEvent actionEvent) {
        JavaScheduling.getInstance().launchCustomerController(selectedCustomer);
    }

    private void handleDeleteCustomer(ActionEvent actionEvent) {
        if (selectedCustomer != null) {
            try {
                JavaSchedulerDatabase.Repository.deleteCustomerById(selectedCustomer.getId());
                selectedCustomer = null;
                populateCustomersChoiceBox();
                customerChoiceBox.getSelectionModel().clearSelection();
                boolean shouldShowControls = false;
                handleCustomerControlsVisibility(shouldShowControls);
            } catch (SQLException | IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void handleCustomerControlsVisibility(boolean shouldShowControls) {
        deleteButton.setVisible(shouldShowControls);
        editCustomerButton.setVisible(shouldShowControls);
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setSelectedCustomer(Customer selectedCustomer) {
        if (selectedCustomer != null) {
            this.selectedCustomer = customers.filtered(customer -> customer.getId() == selectedCustomer.getId()).get(0); // use of lambda to simplify predicate
            ChoiceBoxItemMap choiceRef = customerChoiceBox.getItems().filtered(choice -> choice.getKey() == selectedCustomer.getId()).get(0); // use of lambda to simplify predicate
            customerChoiceBox.getSelectionModel().select(choiceRef);
        }
    }

    private void populateCustomersChoiceBox() {
        try {
            customers.setAll(JavaSchedulerDatabase.Repository.getAllCustomers());
        } catch (SQLException | IOException e) {
            e.printStackTrace();
        }
    }

    private void cancelHandler(ActionEvent event) {
        JavaScheduling.getInstance().launchMain(user);
    }

    private void addNewCustomerHandler(ActionEvent event) {
        JavaScheduling.getInstance().launchCustomerController(null);
    }

    private void populateDurationChoiceBox() {
        List<ChoiceBoxItemMap> durations = new ArrayList<>();
        durations.add(new ChoiceBoxItemMap(14, "Fifteen Minutes"));
        durations.add(new ChoiceBoxItemMap(29, "Thirty Minutes"));
        durations.add(new ChoiceBoxItemMap(44, "Forty-five Minutes"));
        durations.add(new ChoiceBoxItemMap(59, "One Hour"));
        this.appointmentDurationChoiceBox.setItems(FXCollections.observableList(durations));
    }

    private void populateTimeChoiceBox() {
        this.appointmentTimeChoiceBox.setItems(FXCollections.observableList(AppointmentTimeFactory.getTimeSlots()));
    }


    private class CustomerListChangeListener implements ListChangeListener<Customer> {

        @Override
        public void onChanged(Change<? extends Customer> c) {
            List<ChoiceBoxItemMap> choices = new ArrayList<>();
            customers.forEach(customer -> choices.add(new ChoiceBoxItemMap(customer.getId(), customer.getCustomerName()))); // use of lambda to simplify foreach loop action
            customerChoiceBox.setItems(FXCollections.observableList(choices));
        }
    }

}
