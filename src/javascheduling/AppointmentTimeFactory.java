package javascheduling;

import java.util.ArrayList;
import java.util.List;

public class AppointmentTimeFactory {
    private static int[] workingHours = new int[]{8, 9, 10, 11, 12, 1, 2, 3, 4};
    private static String[] endingOptions = new String[]{":00", ":15", ":30", ":45"};
    private static List<ChoiceBoxItemMap> memoizedItems = null;

    public static List<ChoiceBoxItemMap> getTimeSlots() {
        if (memoizedItems != null) {
            return memoizedItems;
        }
        List<ChoiceBoxItemMap> items = new ArrayList<>();
        int indexer = 0;
        for (int hour :
                workingHours) {
            for (int i = 0; i < 4; i++) {
                items.add(new ChoiceBoxItemMap(indexer, hour + endingOptions[indexer % 4]));
                indexer++;
            }
        }
        memoizedItems = items;
        return memoizedItems;
    }
}
