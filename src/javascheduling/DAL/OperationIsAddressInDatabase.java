package javascheduling.DAL;

import javascheduling.DAL.ScheduleContract.AddressContract;

/**
 *
 * @author michael.evanson
 */
class OperationIsAddressInDatabase extends BooleanOperationBase {

    OperationIsAddressInDatabase(int addressId) {
        super(AddressContract.ADDRESS_TABLE_NAME, AddressContract.ID_COLUMN);
        addParams(addressId);
    }   
}
