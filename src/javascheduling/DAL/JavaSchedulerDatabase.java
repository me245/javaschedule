package javascheduling.DAL;

import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.List;

import javascheduling.DAL.models.*;

import static javascheduling.DAL.ScheduleContract.*;

/**
 *
 * @author michael.evanson
 */
public class JavaSchedulerDatabase {
    public static class Repository {
        //Scalar By ID Operations
        public static Country getCountryById(int countryId) throws SQLException, IOException {
            OperationGetCountryById operation = new OperationGetCountryById(countryId);
            return operation.execute().get(0);
        }

        public static City getCityById(int cityId) throws SQLException, IOException {
            OperationGetCityById operation = new OperationGetCityById(cityId);
            return operation.execute().get(0);
        }

        public static Address getAddressById(int addressId) throws SQLException, IOException {
            OperationGetAddressById operation = new OperationGetAddressById(addressId);
            return operation.execute().get(0);
        }

        public static Customer getCustomerById(int customerId) throws SQLException, IOException {
            OperationGetCustomerById operation = new OperationGetCustomerById(customerId);
            return operation.execute().get(0);
        }

        public static Appointment getAppointmentById(int appointmentId) throws SQLException, IOException {
            OperationGetAppointmentById operation = new OperationGetAppointmentById(appointmentId);
            return operation.execute().get(0);
        }

        public static User getUserById(int userId) throws SQLException, IOException {
            OperationGetUserById operation = new OperationGetUserById(userId);
            return operation.execute().get(0);
        }

        //End Scalar By ID Operations
        public static User getUserByName(String userName) throws SQLException, IOException {
            OperationGetUserByName operation = new OperationGetUserByName(userName);
            List<User> matchingUsers = operation.execute();
            return matchingUsers.size() > 0 ? matchingUsers.get(0) : null;
        }

        //getAll* Operations
        public static List<Country> getAllCountries() throws SQLException, IOException {
            OperationGetAllCountries operation = new OperationGetAllCountries();
            return operation.execute();
        }

        public static List<City> getAllCitiesFromCountry(int countryId) throws SQLException, IOException {
            OperationGetAllCitiesFromCountry operation = new OperationGetAllCitiesFromCountry(countryId);
            return operation.execute();
        }

        public static List<Customer> getAllCustomers() throws SQLException, IOException {
            OperationGetAllCustomers operation = new OperationGetAllCustomers();
            return operation.execute();
        }

        public static List<Appointment> getAllAppointments() throws SQLException, IOException {
            OperationGetAllAppointments operation = new OperationGetAllAppointments();
            return operation.execute();
        }

        public static List<Appointment> getAllAppointmentsByDateRangeAndUser(LocalDateTime fromDate, LocalDateTime toDate, int userId) throws IOException, SQLException {
            OperationGetAllAppointmentsByDateRangeAndUser operation = new OperationGetAllAppointmentsByDateRangeAndUser(fromDate, toDate, userId);
            return operation.execute();
        }

        public static List<User> getAllUsers() throws SQLException, IOException {
            OperationGetAllUsers operation = new OperationGetAllUsers();
            return operation.execute();
        }

        //End getAll* Operations
        //boolean is*InDatabase Operations
        public static boolean isCountryInDatabase(int countryId) throws SQLException, IOException {
            OperationIsCountryInDatabase operation = new OperationIsCountryInDatabase(countryId);
            return operation.execute();
        }

        public static boolean isCityInDatabase(int cityId) throws SQLException, IOException {
            OperationIsCityInDatabase operation = new OperationIsCityInDatabase(cityId);
            return operation.execute();
        }

        public static boolean isAddressInDatabase(int addressId) throws SQLException, IOException {
            OperationIsAddressInDatabase operation = new OperationIsAddressInDatabase(addressId);
            return operation.execute();
        }

        public static boolean isCustomerInDatabase(int customerId) throws SQLException, IOException {
            OperationIsCustomerInDatabase operation = new OperationIsCustomerInDatabase(customerId);
            return operation.execute();
        }

        public static boolean isAppointmentInDatabase(int appointmentId) throws SQLException, IOException {
            OperationIsAppointmentInDatabase operation = new OperationIsAppointmentInDatabase(appointmentId);
            return operation.execute();
        }

        public static boolean isUserInDatabase(int userId) throws SQLException, IOException {
            OperationIsUserInDatabase operation = new OperationIsUserInDatabase(userId);
            return operation.execute();
        }

        //End boolean is*InDatabase Operations
        //Integer updateOrInsert* Operations
        public static int updateOrInsertCountry(Country country) throws SQLException, IOException {
            OperationUpdateOrInsertCountry operation = new OperationUpdateOrInsertCountry(country);
            return operation.execute();
        }

        public static int updateOrInsertCity(City city) throws SQLException, IOException {
            OperationUpdateOrInsertCity operation = new OperationUpdateOrInsertCity(city);
            return operation.execute();
        }

        public static int updateOrInsertAddress(Address address) throws SQLException, IOException {
            OperationUpdateOrInsertAddress operation = new OperationUpdateOrInsertAddress(address);
            return operation.execute();
        }

        public static int updateOrInsertCustomer(Customer customer) throws SQLException, IOException {
            OperationUpdateOrInsertCustomer operation = new OperationUpdateOrInsertCustomer(customer);
            return operation.execute();
        }

        public static int updateOrInsertAppointment(Appointment appointment) throws SQLException, IOException {
            OperationUpdateOrInsertAppointment operation = new OperationUpdateOrInsertAppointment(appointment);
            return operation.execute();
        }

        public static int updateOrInsertUser(User user) throws SQLException, IOException {
            OperationUpdateOrInsertUser operation = new OperationUpdateOrInsertUser(user);
            return operation.execute();
        }
        //End Integer updateOrInsert* Operations
        //Boolean Delete*ById Operations
        public static boolean deleteCountryById(int countryId) throws SQLException, IOException {
            OperationDeleteById operation = new OperationDeleteById(CountryContract.COUNTRY_TABLE_NAME, CountryContract.ID_COLUMN, countryId);
            return operation.execute();
        }

        public static boolean deleteCityById(int cityId) throws SQLException, IOException {
            OperationDeleteById operation = new OperationDeleteById(CityContract.CITY_TABLE_NAME, CityContract.ID_COLUMN, cityId);
            return operation.execute();
        }

        public static boolean deleteAddressById(int addressId) throws SQLException, IOException {
            OperationDeleteById operation = new OperationDeleteById(AddressContract.ADDRESS_TABLE_NAME, AddressContract.ID_COLUMN, addressId);
            return operation.execute();
        }

        public static boolean deleteCustomerById(int customerId) throws SQLException, IOException {
            OperationSoftDeleteCustomer operation = new OperationSoftDeleteCustomer(customerId);
            return operation.execute() > 0;
        }

        public static boolean deleteAppointmentById(int appointmentId) throws SQLException, IOException {
            OperationDeleteById operation = new OperationDeleteById(AppointmentContract.APPOINTMENT_TABLE_NAME, AppointmentContract.ID_COLUMN, appointmentId);
            return operation.execute();
        }

        public static boolean deleteUserById(int userId) throws SQLException, IOException {
            OperationDeleteById operation = new OperationDeleteById(UserContract.USER_TABLE_NAME, UserContract.ID_COLUMN, userId);
            return operation.execute();
        }

        //End Boolean Delete*ById Operations
        //Custom Report Operations
        public static List<AppointmentTypeReport> getCountOfAppointmentTypesEachMonth(int fourDigitYear, int month) throws SQLException, IOException {
            if (month < 0 || month > 12) {
                throw new IllegalArgumentException("Month must be between 0 and 12 inclusive (0 for all months, 1 for January, 12 for December)");
            }
            OperationGetAppointmentTypeCountEachMonthByYear operation = new OperationGetAppointmentTypeCountEachMonthByYear(fourDigitYear, month);
            return operation.execute();
        }

        public static int getCountOfActiveCustomers() throws SQLException, IOException {
            OperationGetActiveCustomerCount operation = new OperationGetActiveCustomerCount();
            return operation.execute();
        }

        public static List<ConsultantSchedule> getConsultantSchedules() throws SQLException, IOException {
            OperationGetConsultantSchedules operation = new OperationGetConsultantSchedules();
            return operation.execute();
        }

        //End Custom Report Operations
    }
}
