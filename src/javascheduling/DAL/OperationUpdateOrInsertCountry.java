package javascheduling.DAL;

import javascheduling.DAL.ScheduleContract.CountryContract;
import javascheduling.DAL.models.Country;

/**
 *
 * @author michael.evanson
 */
class OperationUpdateOrInsertCountry extends UpdateOrInsertOperationBase {

    OperationUpdateOrInsertCountry(Country country) {
        super(country);
        setTableInformation(CountryContract.COUNTRY_TABLE_NAME, CountryContract.ID_COLUMN);
    }

    @Override
    protected void setMetaData() {
    }
}
