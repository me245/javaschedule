package javascheduling.DAL;

import java.io.IOException;
import java.sql.ResultSet;
import java.util.List;

/**
 *
 * @author michael.evanson
 */
interface IScheduleMapper<T> {
    List<T> MapAll(ResultSet resultSet) throws IOException;
}
