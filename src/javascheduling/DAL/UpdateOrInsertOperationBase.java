package javascheduling.DAL;

import javascheduling.DAL.models.IDBModel;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.StringJoiner;

/**
 * @author michael.evanson
 */
abstract class UpdateOrInsertOperationBase extends OperationBase {
    private String tableName;
    private String idColumn;
    private final Map<String, Object> keyedParams;
    protected final IDBModel model;

    UpdateOrInsertOperationBase(IDBModel model) {
        this.keyedParams = new HashMap<>();
        this.model = model;
    }

    protected final void addKeyedParams(String key, Object value) {
        keyedParams.put(key, value);
    }

    protected abstract void setMetaData();

    protected final void setTableInformation(String tableName, String idColumn) {
        this.tableName = tableName;
        this.idColumn = idColumn;
    }

    @Override
    protected Integer execute() throws SQLException, IOException {
        if (tableName == null || tableName.isEmpty() || idColumn == null || idColumn.isEmpty()){
            throw new IllegalStateException("You must call setTableInformation before execute");
        }
        try {
            reverseMap();
        } catch (InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
            return -1;
        }
        setMetaData();
        String sql = getCommand();
        Connection connection = getConnection();
        PreparedStatement preparedStatement = buildPreparedStatementWithParameters(connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS));
        int rows = preparedStatement.executeUpdate();
        ResultSet resultSet = preparedStatement.getGeneratedKeys();
        if (resultSet.next()) {
            return resultSet.getInt(1);
        } else {
            return -1;
        }
    }

    private void reverseMap() throws InvocationTargetException, IllegalAccessException {
        Method[] methods = model.getClass().getDeclaredMethods();
        for (Method method : methods) {
            if(!method.getReturnType().equals(Void.TYPE) && method.getExceptionTypes().length == 0) {
                String paramName = method.getName().substring(3);
                addKeyedParams(paramName, method.invoke(model));
            }
        }
    }

    private String getCommand() {
        StringBuilder commandSql = new StringBuilder();
        if (model.getId() > 0) {
            commandSql.append("UPDATE ").append(tableName).append(" SET ");
            StringJoiner sj = new StringJoiner(", ");
            for (String key : keyedParams.keySet()) {
                if (key.equalsIgnoreCase("id")) {
                    addParams(keyedParams.get(key));
                    continue;
                }
                String value = getStringEquivalentFromParameters(key);
                sj.add(String.format("%s = %s", key, value));
            }
            commandSql.append(sj.toString()).append(" WHERE ").append(idColumn).append(" = ?;");
        } else {
            commandSql.append("INSERT INTO ").append(tableName).append("(");
            StringJoiner sjColumns = new StringJoiner(", ");
            StringJoiner sjValues = new StringJoiner(", ");
            for (String key : keyedParams.keySet()) {
                if (key.equalsIgnoreCase("id")) {
                    continue;
                }
                String value = getStringEquivalentFromParameters(key);
                sjColumns.add(key);
                sjValues.add(value);
            }
            commandSql.append(sjColumns.toString()).append(") VALUES (").append(sjValues.toString()).append(");");
        }
        return commandSql.toString();
    }

    private String getStringEquivalentFromParameters(String key) {
        Object o = keyedParams.get(key);
        String value;
        if (o.getClass().isPrimitive()) {
            value = String.valueOf(o);
        } else if (o instanceof Boolean) {
            value = String.valueOf(((Boolean) o) ? 1 : 0);
        } else if (o instanceof LocalDateTime) {
            value = "'" + convertToUTCTimestamp((LocalDateTime) o) + "'";
        } else {
            value = "'" + o + "'";
        }
        return value;
    }
}
