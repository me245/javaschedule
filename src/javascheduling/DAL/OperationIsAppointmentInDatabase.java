package javascheduling.DAL;

import javascheduling.DAL.ScheduleContract.AppointmentContract;

/**
 *
 * @author michael.evanson
 */
class OperationIsAppointmentInDatabase extends BooleanOperationBase {

    OperationIsAppointmentInDatabase(int appointmentId) {
        super(AppointmentContract.APPOINTMENT_TABLE_NAME, AppointmentContract.ID_COLUMN);
        addParams(appointmentId);
    }   
}
