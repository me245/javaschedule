package javascheduling.DAL;

/**
 *
 * @author michael.evanson
 */
enum DB {
    LOCAL,
    REMOTE,
    SQLITE;
}
