package javascheduling.DAL;

import javascheduling.DAL.ScheduleContract.UserContract;

/**
 *
 * @author michael.evanson
 */
class OperationIsUserInDatabase extends BooleanOperationBase {

    OperationIsUserInDatabase(int userId) {
        super(UserContract.USER_TABLE_NAME, UserContract.ID_COLUMN);
        addParams(userId);
    }   
}
