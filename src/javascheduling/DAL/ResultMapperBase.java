package javascheduling.DAL;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author michael.evanson
 */
abstract class ResultMapperBase<T> implements IScheduleMapper<T> {

    protected abstract T Map(ResultSet record) throws IOException;

    @Override
    public List<T> MapAll(ResultSet resultSet) throws IOException {
        List<T> collection = new ArrayList<>();

        try {
            while (resultSet.next()) {
                T t = Map(resultSet);
                if(t != null) {
                    collection.add(t);
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(ResultMapperBase.class.getName()).log(Level.SEVERE, null, ex);
        }

        return collection;
    }

    protected LocalDateTime mapDateFromResultSet(ResultSet record, String dateColumn) throws SQLException {
        return ZonedDateTime.of(record.getTimestamp(dateColumn).toLocalDateTime(), ZoneOffset.UTC).withZoneSameInstant(ZoneId.systemDefault()).toLocalDateTime();
    }

}
