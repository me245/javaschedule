package javascheduling.DAL;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author michael.evanson
 */
class DBConfigManager {
    
    public Configuration getConfiguration() throws IOException {
        Configuration config = new Configuration();
        String propFileName = "resources/db.config.properties";
        try(BufferedReader br = new BufferedReader(new InputStreamReader(getClass().getClassLoader().getResourceAsStream(propFileName)))) {
            if(br != null) {
                config.load(br);
            } else {
		throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
            }
        }
        return config;
    }
}
