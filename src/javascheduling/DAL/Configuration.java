package javascheduling.DAL;

import java.security.InvalidParameterException;
import java.text.MessageFormat;
import java.util.Properties;

/**
 *
 * @author michael.evanson
 */
class Configuration extends Properties {
    @Override
    public String getProperty(final String key) {
        final String property = super.getProperty(key);
        if(property == null) {
            throw new InvalidParameterException(MessageFormat.format("Missing value in db.config.properties for key{0}!", key));
        }
        return property;
    }    
}
