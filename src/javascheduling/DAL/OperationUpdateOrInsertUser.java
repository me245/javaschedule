package javascheduling.DAL;

import javascheduling.DAL.ScheduleContract.UserContract;
import javascheduling.DAL.models.User;

/**
 *
 * @author michael.evanson
 */
class OperationUpdateOrInsertUser extends UpdateOrInsertOperationBase {

    OperationUpdateOrInsertUser(User user) {
        super(user);
        setTableInformation(UserContract.USER_TABLE_NAME, UserContract.ID_COLUMN);
    }

    @Override
    protected void setMetaData() {
    }
}
