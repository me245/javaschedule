package javascheduling.DAL;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

class OperationDeleteById extends BooleanOperationBase {
    OperationDeleteById(String table, String idColumn, int id) {
        super(table, idColumn);
        addParams(id);
    }

    @Override
    protected Boolean execute() throws SQLException, IOException {
        Connection connection = getConnection();
        PreparedStatement preparedStatement = buildPreparedStatementWithParameters(connection.prepareStatement(getCommand()));
        return preparedStatement.executeUpdate() > 0;
    }

    @Override
    protected String getCommand() {
        return "DELETE FROM " + tableName +
                " WHERE " + idColumn + " = ?;";
    }
}
