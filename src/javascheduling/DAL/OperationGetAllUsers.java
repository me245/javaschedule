package javascheduling.DAL;

import javascheduling.DAL.ScheduleContract.UserContract;
import javascheduling.DAL.models.User;
/**
 *
 * @author michael.evanson
 */
class OperationGetAllUsers extends ObjectOperationBase<User> {
    
    @Override
    protected ResultMapperBase<User> getMapper() {
        return new UserMapper();
    }

    @Override
    protected String getCommand() {
        return "SELECT " +
                UserContract.ID_COLUMN + ", " +
                UserContract.NAME_COLUMN + ", " +
                UserContract.PASSWORD_COLUMN + ", " +
                UserContract.ACTIVE_COLUMN + ", " +
                UserContract.CREATED_BY_COLUMN + ", " +
                UserContract.CREATE_DATE_COLUMN + ", " +
                UserContract.LAST_UPDATE_BY_COLUMN + ", " +
                UserContract.LAST_UPDATE_COLUMN +
                " FROM " + UserContract.USER_TABLE_NAME +
                ";";
    }
}
