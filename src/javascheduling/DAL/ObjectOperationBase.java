package javascheduling.DAL;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author michael.evanson
 */
abstract class ObjectOperationBase<T> extends OperationBase {
    protected abstract ResultMapperBase<T> getMapper();
    protected abstract String getCommand();
    
    @Override
    protected List<T> execute() throws SQLException, IOException {
        Connection connection = getConnection();
        PreparedStatement preparedStatement = buildPreparedStatementWithParameters(connection.prepareStatement(getCommand()));
        ResultSet resultSet = preparedStatement.executeQuery();
        return getMapper().MapAll(resultSet);
    }
    
    protected int executeUpdate() throws SQLException, IOException {
        Connection connection = getConnection();
        PreparedStatement preparedStatement = buildPreparedStatementWithParameters(connection.prepareStatement(getCommand()));
        return preparedStatement.executeUpdate();
    }
}
