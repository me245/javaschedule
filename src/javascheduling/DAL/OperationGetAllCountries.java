package javascheduling.DAL;

import javascheduling.DAL.ScheduleContract.CountryContract;
import javascheduling.DAL.models.Country;

/**
 *
 * @author michael.evanson
 */
class OperationGetAllCountries extends ObjectOperationBase<Country> {

    OperationGetAllCountries() {
    }

    @Override
    protected ResultMapperBase<Country> getMapper() {
        return new CountryMapper();
    }

    @Override
    protected String getCommand() {
        return "SELECT " +
                CountryContract.ID_COLUMN + ", " +
                CountryContract.NAME_COLUMN + ", " +
                CountryContract.CREATE_BY_COLUMN + ", " +
                CountryContract.CREATE_DATE_COLUMN + ", " +
                CountryContract.LAST_UPDATE_BY_COLUMN + ", " +
                CountryContract.LAST_UPDATE_COLUMN +
                " FROM " + CountryContract.COUNTRY_TABLE_NAME +
                ";";
    }
    
}
