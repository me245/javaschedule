package javascheduling.DAL;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.security.InvalidParameterException;
import java.sql.*;
import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author michael.evanson
 */
abstract class OperationBase {
    private final List<Object> params = new ArrayList<>();
    private boolean areParamsSet = false;
    
    protected final void addParams(Object o) {
        this.params.add(o);
    }
    
    protected PreparedStatement buildPreparedStatementWithParameters(PreparedStatement statementToAddParamsTo) throws SQLException {
        ParameterMetaData metaData = statementToAddParamsTo.getParameterMetaData();
        if (metaData.getParameterCount() != this.params.size()) {
            throw new InvalidParameterException(MessageFormat.format("Incorrect number of Parameters added to method. Expected {0} got {1}", metaData.getParameterCount(), params.size()));
        }
        for(int i = 0; i < this.params.size(); i++) {
            if(this.params.get(i) instanceof Integer) {
                statementToAddParamsTo.setInt(i + 1, (Integer)this.params.get(i));
            } else if(this.params.get(i) instanceof Double) {
                statementToAddParamsTo.setDouble(i + 1, (Double)this.params.get(i));
            } else if(this.params.get(i) instanceof String) {
                statementToAddParamsTo.setString(i + 1, (String)this.params.get(i));
            } else if (this.params.get(i) instanceof Timestamp) {
                statementToAddParamsTo.setTimestamp(i + 1, (Timestamp) this.params.get(i));
            } else if (this.params.get(i) instanceof Boolean) {
                statementToAddParamsTo.setInt(i + 1, (Boolean) this.params.get(i) ? 1 : 0);
            } else if (this.params.get(i) instanceof LocalDateTime) {
                statementToAddParamsTo.setTimestamp(i + 1, convertToUTCTimestamp((LocalDateTime) this.params.get(i)));
            } else {
                throw new InvalidParameterException(MessageFormat.format("Wrong Parameter added to method. Expected {0} got {1}", metaData.getParameterType(i + 1), this.params.get(i).getClass()));
            }
        }
        this.areParamsSet = true;
        return statementToAddParamsTo;
    }

    protected Timestamp convertToUTCTimestamp(LocalDateTime dateTime) {
        ZonedDateTime converted = dateTime.atZone(ZoneId.systemDefault()).withZoneSameInstant(ZoneOffset.UTC);
        return Timestamp.valueOf(converted.toLocalDateTime());
    }

    protected boolean isParamsSet() {
        return this.areParamsSet;
    }

    protected abstract Object execute() throws SQLException, IOException;

    protected Connection getConnection() throws SQLException, IOException {
        Configuration dbConfig = new DBConfigManager().getConfiguration();
        String dbType = dbConfig.getProperty("db");
        DB db = DB.valueOf(dbType.toUpperCase());
        switch(db) {
            case LOCAL:
                return getLocalConnection(dbConfig);
            case REMOTE:
                return getRemoteConnection(dbConfig);
            case SQLITE:
                return getSQLiteConnection(dbConfig);
            default:
                throw new InvalidParameterException(MessageFormat.format("Incorrect value in db.config.properties for key{0}!", "db"));
        }
    }
    
    protected boolean isDatabaseInitialized() throws SQLException, IOException {
        Connection conn = this.getConnection();
        DatabaseMetaData meta = conn.getMetaData();
        for(Class table : ScheduleContract.class.getClasses()) {
            Method tableName = null;
            try {
                tableName = table.getDeclaredMethod("getTableName");
            } catch (NoSuchMethodException | SecurityException ex) {
                Logger.getLogger(OperationBase.class.getName()).log(Level.SEVERE, null, ex);
            }
            if(tableName == null) {
                return false;
            }
            try {
                ResultSet result = meta.getTables(null, null, (String) tableName.invoke(table), null);
                if(!result.isBeforeFirst()) {
                    return false;
                }
            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                Logger.getLogger(OperationBase.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return true;
    }

    private Connection getLocalConnection(Configuration config) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    private Connection getRemoteConnection(Configuration config) throws SQLException {
        return DriverManager.getConnection(config.getProperty("remoteUrl"), config.getProperty("remoteUser"), config.getProperty("remotePass"));
    }

    private Connection getSQLiteConnection(Configuration config) throws SQLException {
        return DriverManager.getConnection(config.getProperty("sqlliteUrl"));
    }
}
