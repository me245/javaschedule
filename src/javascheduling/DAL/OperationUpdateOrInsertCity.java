package javascheduling.DAL;

import javascheduling.DAL.ScheduleContract.CityContract;
import javascheduling.DAL.models.City;

/**
 *
 * @author michael.evanson
 */
class OperationUpdateOrInsertCity extends UpdateOrInsertOperationBase {
    OperationUpdateOrInsertCity(City city) {
        super(city);
        setTableInformation(CityContract.CITY_TABLE_NAME, CityContract.ID_COLUMN);
    }

    @Override
    protected void setMetaData() {
    }
}
