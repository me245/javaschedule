package javascheduling.DAL;

import javascheduling.DAL.models.AppointmentTypeReport;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

public class AppointmentTypeReportMapper extends ResultMapperBase<AppointmentTypeReport> {
    @Override
    protected AppointmentTypeReport Map(ResultSet record) throws IOException {
        try {
            int monthNumber = record.getInt(1);
            int typeCount = record.getInt(2);
            return new AppointmentTypeReport(monthNumber, typeCount);
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
}
