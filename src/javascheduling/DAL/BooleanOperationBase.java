package javascheduling.DAL;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author michael.evanson
 */
abstract class BooleanOperationBase extends OperationBase {
    final String tableName;
    final String idColumn;
    
    public BooleanOperationBase(String table, String idColumn) {
        this.tableName = table;
        this.idColumn = idColumn;
    }
    
    @Override
    protected Boolean execute() throws SQLException, IOException {
        Connection connection = getConnection();
        PreparedStatement preparedStatement = buildPreparedStatementWithParameters(connection.prepareStatement(getCommand()));
        ResultSet resultSet = preparedStatement.executeQuery();
        return resultSet.next() && resultSet.getBoolean(1);
    }

    protected String getCommand() {
        return "SELECT EXISTS(" +
                "SELECT 1 FROM " + tableName +
                "WHERE " + idColumn + " = ?);";
    }
}
