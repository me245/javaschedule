package javascheduling.DAL;

import javascheduling.DAL.ScheduleContract.CustomerContract;

/**
 *
 * @author michael.evanson
 */
class OperationIsCustomerInDatabase extends BooleanOperationBase {

    OperationIsCustomerInDatabase(int customerId) {
        super(CustomerContract.CUSTOMER_TABLE_NAME, CustomerContract.ID_COLUMN);
        addParams(customerId);
    }   
}
