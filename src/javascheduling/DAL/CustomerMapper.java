package javascheduling.DAL;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javascheduling.DAL.ScheduleContract.CustomerContract;
import javascheduling.DAL.models.Address;
import javascheduling.DAL.models.Customer;
/**
 *
 * @author michael.evanson
 */
class CustomerMapper extends ResultMapperBase<Customer> {
    
    @Override
    protected Customer Map(ResultSet record) {
        try {
            int id = record.getInt(CustomerContract.ID_COLUMN);
            int addressId = record.getInt(CustomerContract.ADDRESS_ID_COLUMN);
            boolean active = record.getBoolean(CustomerContract.ACTIVE_COLUMN);
            LocalDateTime createDate = mapDateFromResultSet(record, CustomerContract.CREATE_DATE_COLUMN);
            LocalDateTime lastUpdate = mapDateFromResultSet(record, CustomerContract.LAST_UPDATE_COLUMN);
            String customerName = record.getString(CustomerContract.NAME_COLUMN);
            String createBy = record.getString(CustomerContract.CREATE_BY_COLUMN);
            String lastUpdateBy = record.getString(CustomerContract.LAST_UPDATE_BY_COLUMN);
            Customer customer = new Customer(id);
            customer.setAddressId(addressId);
            customer.setActive(active);
            customer.setCustomerName(customerName);
            customer.setCreatedBy(createBy);
            customer.setLastUpdateBy(lastUpdateBy);
            customer.setCreateDate(createDate);
            customer.setLastUpdate(lastUpdate);
            return customer;
        } catch (SQLException ex) {
            Logger.getLogger(UserMapper.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
}
