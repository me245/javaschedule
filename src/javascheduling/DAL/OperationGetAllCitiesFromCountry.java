package javascheduling.DAL;

import javascheduling.DAL.ScheduleContract.CityContract;
import javascheduling.DAL.models.City;

/**
 *
 * @author michael.evanson
 */
class OperationGetAllCitiesFromCountry extends ObjectOperationBase<City> {

    OperationGetAllCitiesFromCountry(int countryId) {
        addParams(countryId);
    }
     
    @Override
    protected ResultMapperBase<City> getMapper() {
        return new CityMapper();
    }

    @Override
    protected String getCommand() {
        return "SELECT " +
                CityContract.ID_COLUMN + ", " +
                CityContract.COUNTRY_ID_COLUMN + ", " +
                CityContract.NAME_COLUMN + ", " +
                CityContract.CREATE_BY_COLUMN + ", " +
                CityContract.CREATE_DATE_COLUMN + ", " +
                CityContract.LAST_UPDATE_BY_COLUMN + ", " +
                CityContract.LAST_UPDATE_COLUMN +
                " FROM " + CityContract.CITY_TABLE_NAME +
                " WHERE " + CityContract.COUNTRY_ID_COLUMN +
                " = ?;";
    }
}
