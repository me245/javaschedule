package javascheduling.DAL;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javascheduling.DAL.ScheduleContract.AppointmentContract;
import javascheduling.DAL.models.Appointment;

/**
 *
 * @author michael.evanson
 */
class AppointmentMapper extends ResultMapperBase<Appointment> {
    
    @Override
    protected Appointment Map(ResultSet record) {
        try {
            int id = record.getInt(AppointmentContract.ID_COLUMN);
            int customerId = record.getInt(AppointmentContract.CUSTOMER_ID_COLUMN);
            int userId = record.getInt(AppointmentContract.USER_ID_COLUMN);
            LocalDateTime createDate = mapDateFromResultSet(record, AppointmentContract.CREATE_DATE_COLUMN);
            LocalDateTime lastUpdate = mapDateFromResultSet(record, AppointmentContract.LAST_UPDATE_COLUMN);
            LocalDateTime start = mapDateFromResultSet(record, AppointmentContract.START_COLUMN);
            LocalDateTime end = mapDateFromResultSet(record, AppointmentContract.END_COLUMN);
            String title = record.getString(AppointmentContract.TITLE_COLUMN);
            String description = record.getString(AppointmentContract.DESCRIPTION_COLUMN);
            String location = record.getString(AppointmentContract.LOCATION_COLUMN);
            String contact = record.getString(AppointmentContract.CONTACT_COLUMN);
            String type = record.getString(AppointmentContract.TYPE_COLUMN);
            String url = record.getString(AppointmentContract.URL_COLUMN);
            String createBy = record.getString(AppointmentContract.CREATE_BY_COLUMN);
            String lastUpdateBy = record.getString(AppointmentContract.LAST_UPDATE_BY_COLUMN);
            Appointment appointment = new Appointment(id);
            appointment.setCustomerId(customerId);
            appointment.setUserId(userId);
            appointment.setTitle(title);
            appointment.setDescription(description);
            appointment.setLocation(location);
            appointment.setContact(contact);
            appointment.setType(type);
            appointment.setUrl(url);
            appointment.setStart(start);
            appointment.setEnd(end);
            appointment.setCreatedBy(createBy);
            appointment.setLastUpdateBy(lastUpdateBy);
            appointment.setCreateDate(createDate);
            appointment.setLastUpdate(lastUpdate);
            return appointment;
        } catch (SQLException ex) {
            Logger.getLogger(UserMapper.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
}
    
}
