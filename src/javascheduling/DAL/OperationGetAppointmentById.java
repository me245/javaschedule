package javascheduling.DAL;

import javascheduling.DAL.ScheduleContract.AppointmentContract;
import javascheduling.DAL.models.Appointment;

/**
 *
 * @author michael.evanson
 */
class OperationGetAppointmentById extends ObjectOperationBase<Appointment> {

    OperationGetAppointmentById(int appointmentId) {
        addParams(appointmentId);
    }

    @Override
    protected ResultMapperBase<Appointment> getMapper() {
        return new AppointmentMapper();
    }

    @Override
    protected String getCommand() {
        return "SELECT " +
                AppointmentContract.ID_COLUMN + ", " +
                AppointmentContract.CUSTOMER_ID_COLUMN + ", " +
                AppointmentContract.TITLE_COLUMN + ", " +
                AppointmentContract.DESCRIPTION_COLUMN + ", " +
                AppointmentContract.LOCATION_COLUMN + ", " +
                AppointmentContract.CONTACT_COLUMN + ", " +
                AppointmentContract.URL_COLUMN + ", " +
                AppointmentContract.START_COLUMN + ", " +
                AppointmentContract.END_COLUMN + ", " +
                AppointmentContract.CREATE_BY_COLUMN + ", " +
                AppointmentContract.CREATE_DATE_COLUMN + ", " +
                AppointmentContract.LAST_UPDATE_BY_COLUMN + ", " +
                AppointmentContract.LAST_UPDATE_COLUMN +
                " FROM " + AppointmentContract.APPOINTMENT_TABLE_NAME +
                " WHERE " + AppointmentContract.ID_COLUMN +
                " = ?;";
    }
    
}
