package javascheduling.DAL;

/**
 *
 * @author michael.evanson
 */
class ScheduleContract {
    public static class CountryContract {
        public static final String COUNTRY_TABLE_NAME = "country";
        public static final String ID_COLUMN = "countryId";
        public static final String NAME_COLUMN = "country";
        public static final String CREATE_DATE_COLUMN = "createDate";
        public static final String CREATE_BY_COLUMN = "createdBy";
        public static final String LAST_UPDATE_COLUMN = "lastUpdate";
        public static final String LAST_UPDATE_BY_COLUMN = "lastUpdateBy";
        public static String getTableName() { return COUNTRY_TABLE_NAME; }
    }
    public static class CityContract {
        public static final String CITY_TABLE_NAME = "city";
        public static final String ID_COLUMN = "cityId";
        public static final String NAME_COLUMN = "city";
        public static final String COUNTRY_ID_COLUMN = "countryId";
        public static final String CREATE_DATE_COLUMN = "createDate";
        public static final String CREATE_BY_COLUMN = "createdBy";
        public static final String LAST_UPDATE_COLUMN = "lastUpdate";
        public static final String LAST_UPDATE_BY_COLUMN = "lastUpdateBy";
        public static String getTableName() { return CITY_TABLE_NAME; }
    }
    public static class AddressContract {
        public static final String ADDRESS_TABLE_NAME = "address";
        public static final String ID_COLUMN = "addressId";
        public static final String ADDRESS_COLUMN = "address";
        public static final String ADDRESS2_COLUMN = "address2";
        public static final String CITY_ID_COLUMN = "cityId";
        public static final String POSTAL_CODE_COLUMN = "postalCode";
        public static final String PHONE_COLUMN = "phone";
        public static final String CREATE_DATE_COLUMN = "createDate";
        public static final String CREATE_BY_COLUMN = "createdBy";
        public static final String LAST_UPDATE_COLUMN = "lastUpdate";
        public static final String LAST_UPDATE_BY_COLUMN = "lastUpdateBy";
        public static String getTableName() { return ADDRESS_TABLE_NAME; }
    }
    public static class CustomerContract {
        public static final String CUSTOMER_TABLE_NAME = "customer";
        public static final String ID_COLUMN = "customerId";
        public static final String NAME_COLUMN = "customerName";
        public static final String ADDRESS_ID_COLUMN = "addressId";
        public static final String ACTIVE_COLUMN = "active";
        public static final String CREATE_DATE_COLUMN = "createDate";
        public static final String CREATE_BY_COLUMN = "createdBy";
        public static final String LAST_UPDATE_COLUMN = "lastUpdate";
        public static final String LAST_UPDATE_BY_COLUMN = "lastUpdateBy";
        public static String getTableName() { return CUSTOMER_TABLE_NAME; }
    }
    public static class AppointmentContract {
        public static final String APPOINTMENT_TABLE_NAME = "appointment";
        public static final String ID_COLUMN = "appointmentId";
        public static final String CUSTOMER_ID_COLUMN = "customerId";
        public static final String USER_ID_COLUMN = "userId";
        public static final String TITLE_COLUMN = "title";
        public static final String DESCRIPTION_COLUMN = "description";
        public static final String LOCATION_COLUMN = "location";
        public static final String CONTACT_COLUMN = "contact";
        public static final String TYPE_COLUMN = "type";
        public static final String URL_COLUMN = "url";
        public static final String START_COLUMN = "start";
        public static final String END_COLUMN = "end";
        public static final String CREATE_DATE_COLUMN = "createDate";
        public static final String CREATE_BY_COLUMN = "createdBy";
        public static final String LAST_UPDATE_COLUMN = "lastUpdate";
        public static final String LAST_UPDATE_BY_COLUMN = "lastUpdateBy";
        public static String getTableName() { return APPOINTMENT_TABLE_NAME; }
    }
    public static class UserContract {
        public static final String USER_TABLE_NAME = "user";
        public static final String ID_COLUMN = "userId";
        public static final String NAME_COLUMN = "userName";
        public static final String PASSWORD_COLUMN = "password";
        public static final String ACTIVE_COLUMN = "active";
        public static final String CREATE_DATE_COLUMN = "createDate";
        public static final String CREATED_BY_COLUMN = "createdBy";
        public static final String LAST_UPDATE_COLUMN = "lastUpdate";
        public static final String LAST_UPDATE_BY_COLUMN = "lastUpdateBy";
        public static String getTableName() { return USER_TABLE_NAME; }
    }
}
