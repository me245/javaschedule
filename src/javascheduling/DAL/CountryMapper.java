package javascheduling.DAL;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javascheduling.DAL.ScheduleContract.CountryContract;
import javascheduling.DAL.models.Country;

/**
 *
 * @author michael.evanson
 */
class CountryMapper extends ResultMapperBase<Country> {

    @Override
    protected Country Map(ResultSet record) {
        try {
            int id = record.getInt(CountryContract.ID_COLUMN);
            LocalDateTime createDate = mapDateFromResultSet(record, CountryContract.CREATE_DATE_COLUMN);
            LocalDateTime lastUpdate = mapDateFromResultSet(record, CountryContract.LAST_UPDATE_COLUMN);
            String countryName = record.getString(CountryContract.NAME_COLUMN);
            String createBy = record.getString(CountryContract.CREATE_BY_COLUMN);
            String lastUpdateBy = record.getString(CountryContract.LAST_UPDATE_BY_COLUMN);
            Country country = new Country(id);
            country.setCountry(countryName);
            country.setCreatedBy(createBy);
            country.setLastUpdateBy(lastUpdateBy);
            country.setCreateDate(createDate);
            country.setLastUpdate(lastUpdate);
            return country;
        } catch (SQLException ex) {
            Logger.getLogger(UserMapper.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }    
}
