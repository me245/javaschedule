package javascheduling.DAL;

import javascheduling.DAL.models.Appointment;
import javascheduling.DAL.models.ConsultantSchedule;
import javascheduling.DAL.models.User;

import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class OperationGetConsultantSchedules extends OperationBase {
    private DateTimeFormatter readableDateFormatter = DateTimeFormatter.ofPattern("MMM d uuuu HH:mm");

    @Override
    protected List<ConsultantSchedule> execute() throws SQLException, IOException {
        List<User> allUsers = JavaSchedulerDatabase.Repository.getAllUsers();
        List<ConsultantSchedule> schedules = new ArrayList<>();
        for (User u : allUsers) {
            List<String> appointmentSummaries = new ArrayList<>();
            ConsultantSchedule userSchedule = new ConsultantSchedule();
            userSchedule.setConsultantName(u.getUserName());
            LocalDateTime today = LocalDateTime.now().toLocalDate().atStartOfDay();
            LocalDateTime maxDate = today.plusYears(5);
            List<Appointment> usersAppointments = JavaSchedulerDatabase.Repository.getAllAppointmentsByDateRangeAndUser(today, maxDate, u.getId());
            for (Appointment appointment : usersAppointments) {
                appointmentSummaries.add(readableDateFormatter.format(appointment.getStart()) + " - " + readableDateFormatter.format(appointment.getEnd()));
            }
            userSchedule.setAppointments(appointmentSummaries);
            schedules.add(userSchedule);
        }
        return schedules;
    }
}
