package javascheduling.DAL;

import javascheduling.DAL.ScheduleContract.CustomerContract;
import javascheduling.DAL.models.Customer;

/**
 *
 * @author michael.evanson
 */
class OperationGetAllCustomers extends ObjectOperationBase<Customer> {
    
    @Override
    protected ResultMapperBase<Customer> getMapper() {
        return new CustomerMapper();
    }

    @Override
    protected String getCommand() {
        return "SELECT " +
                CustomerContract.ID_COLUMN + ", " +
                CustomerContract.ADDRESS_ID_COLUMN + ", " +
                CustomerContract.NAME_COLUMN + ", " +
                CustomerContract.ACTIVE_COLUMN + ", " +
                CustomerContract.CREATE_BY_COLUMN + ", " +
                CustomerContract.CREATE_DATE_COLUMN + ", " +
                CustomerContract.LAST_UPDATE_BY_COLUMN + ", " +
                CustomerContract.LAST_UPDATE_COLUMN +
                " FROM " + CustomerContract.CUSTOMER_TABLE_NAME +
                " WHERE " + CustomerContract.ACTIVE_COLUMN + " = 1;";
    }
}
