package javascheduling.DAL.models;

import java.util.List;

public class ConsultantSchedule {
    private String consultantName;
    private List<String> appointments;

    public String getConsultantName() {
        return consultantName;
    }

    public void setConsultantName(String consultantName) {
        this.consultantName = consultantName;
    }

    public List<String> getAppointments() {
        return appointments;
    }

    public void setAppointments(List<String> appointments) {
        this.appointments = appointments;
    }
}
