package javascheduling.DAL.models;

import javascheduling.DAL.JavaSchedulerDatabase;

import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDateTime;

/**
 * @author michael.evanson
 */
public class Address implements IDBModel {
    private int addressId;
    private String address;
    private String address2;
    private int cityId;
    private City city;
    private String postalCode;
    private String phone;
    private LocalDateTime createDate;
    private String CreatedBy;
    private LocalDateTime lastUpdate;
    private String lastUpdateBy;

    public Address() {
        this(-1);
    }

    public Address(int addressId) {
        this.addressId = addressId;
    }

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    public int getId() {
        return addressId;
    }

    public void setId(int addressId) {
        this.addressId = addressId;
    }

    public City getCity() throws IOException, SQLException {
        if (city != null) {
            return city;
        } else {
            return city = JavaSchedulerDatabase.Repository.getCityById(cityId);
        }
    }

    public void setCity(City city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public LocalDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String CreatedBy) {
        this.CreatedBy = CreatedBy;
    }

    public LocalDateTime getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(LocalDateTime lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getLastUpdateBy() {
        return lastUpdateBy;
    }

    public void setLastUpdateBy(String lastUpdateBy) {
        this.lastUpdateBy = lastUpdateBy;
    }

}
