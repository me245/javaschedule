package javascheduling.DAL.models;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

public interface IDBModel {
    int getId();

    void setId(int id);

    LocalDateTime getCreateDate();

    void setCreateDate(LocalDateTime createDate);

    String getCreatedBy();

    void setCreatedBy(String createdBy);

    LocalDateTime getLastUpdate();

    void setLastUpdate(LocalDateTime lastUpdate);

    String getLastUpdateBy();

    void setLastUpdateBy(String lastUpdateBy);

    default void setUpdateInformation(User user) {
        setLastUpdateBy(user.getUserName());
        setLastUpdate(LocalDateTime.now(ZoneId.systemDefault()));
    }

    default void setCreationInformation(User user) {
        setCreatedBy(user.getUserName());
        setCreateDate(LocalDateTime.now(ZoneId.systemDefault()));
    }
}
