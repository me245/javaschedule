package javascheduling.DAL.models;

import javascheduling.DAL.JavaSchedulerDatabase;

import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * @author michael.evanson
 */
public class City implements IDBModel {
    private int cityId;
    private int countryId;
    private Country country;
    private String city;
    private LocalDateTime createDate;
    private String createdBy;
    private LocalDateTime lastUpdate;
    private String lastUpdateBy;

    public City() {
        this(-1);
    }

    public City(int id) {
        this.cityId = id;
    }

    public int getCountryId() {
        return countryId;
    }

    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }

    public int getId() {
        return cityId;
    }

    public void setId(int cityId) {
        this.cityId = cityId;
    }

    public Country getCountry() throws IOException, SQLException {
        if (country != null) {
            return country;
        } else {
            return country = JavaSchedulerDatabase.Repository.getCountryById(countryId);
        }
    }

    public void setCountry(Country country) {

        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public LocalDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDateTime getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(LocalDateTime lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getLastUpdateBy() {
        return lastUpdateBy;
    }

    public void setLastUpdateBy(String lastUpdateBy) {
        this.lastUpdateBy = lastUpdateBy;
    }
}
