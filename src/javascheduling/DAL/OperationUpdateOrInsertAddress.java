package javascheduling.DAL;


import javascheduling.DAL.ScheduleContract.AddressContract;
import javascheduling.DAL.models.Address;

/**
 *
 * @author michael.evanson
 */
class OperationUpdateOrInsertAddress extends UpdateOrInsertOperationBase {

    OperationUpdateOrInsertAddress(Address address) {
        super(address);
        setTableInformation(AddressContract.ADDRESS_TABLE_NAME, AddressContract.ID_COLUMN);
    }

    @Override
    protected void setMetaData() {

    }
}
