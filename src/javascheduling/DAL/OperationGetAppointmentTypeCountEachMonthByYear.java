package javascheduling.DAL;

import javascheduling.DAL.ScheduleContract.AppointmentContract;
import javascheduling.DAL.models.AppointmentTypeReport;

public class OperationGetAppointmentTypeCountEachMonthByYear extends ObjectOperationBase<AppointmentTypeReport> {
    private boolean includeAllMonths = true;

    public OperationGetAppointmentTypeCountEachMonthByYear(int yearNumber, int monthNumber) {
        addParams(yearNumber);
        if (monthNumber != 0) {
            includeAllMonths = false;
            addParams(monthNumber);
        }
    }

    @Override
    protected ResultMapperBase<AppointmentTypeReport> getMapper() {
        return new AppointmentTypeReportMapper();
    }

    @Override
    protected String getCommand() {
        String optionMonthQuery = includeAllMonths ? "" : " AND WHERE DATE_FORMAT(" + AppointmentContract.START_COLUMN + ", '%m') = ?";
        return "SELECT date_format(" + AppointmentContract.START_COLUMN + ", '%m')" +
                " ,COUNT(DISTINCT " + AppointmentContract.TYPE_COLUMN + ")" +
                " FROM " + AppointmentContract.APPOINTMENT_TABLE_NAME +
                " WHERE DATE_FORMAT(" + AppointmentContract.START_COLUMN + ", '%Y') = ?" +
                optionMonthQuery +
                " GROUP BY DATE_FORMAT(" + AppointmentContract.START_COLUMN + ", '%M');";
    }
}
