package javascheduling.DAL;

import javascheduling.DAL.ScheduleContract.CustomerContract;
import javascheduling.DAL.models.Customer;

/**
 *
 * @author michael.evanson
 */
class OperationUpdateOrInsertCustomer extends UpdateOrInsertOperationBase {

    OperationUpdateOrInsertCustomer(Customer customer) {
        super(customer);
        setTableInformation(CustomerContract.CUSTOMER_TABLE_NAME, CustomerContract.ID_COLUMN);
    }

    @Override
    protected void setMetaData() {

    }
}
