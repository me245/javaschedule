package javascheduling.DAL;

import java.io.IOException;
import java.sql.*;

import javascheduling.DAL.ScheduleContract.CustomerContract;

public class OperationGetActiveCustomerCount extends OperationBase {
    @Override
    protected Integer execute() throws SQLException, IOException {
        String sql = getCommand();
        Connection connection = getConnection();
        PreparedStatement preparedStatement = buildPreparedStatementWithParameters(connection.prepareStatement(sql));
        ResultSet resultSet = preparedStatement.executeQuery();
        if (resultSet.next()) {
            return resultSet.getInt(1);
        }
        return 0;
    }

    private String getCommand() {
        return "SELECT COUNT(*)" +
                " FROM " + CustomerContract.CUSTOMER_TABLE_NAME +
                " WHERE " + CustomerContract.ACTIVE_COLUMN + " = 1;";
    }
}
