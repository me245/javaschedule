package javascheduling.DAL;

import javascheduling.DAL.ScheduleContract.AppointmentContract;
import javascheduling.DAL.models.Appointment;

import java.io.IOException;
import java.sql.*;
import java.time.ZoneId;
import java.time.ZoneOffset;

/**
 * @author michael.evanson
 */
class OperationUpdateOrInsertAppointment extends OperationBase {
    private Appointment appointment;
    private boolean isInsert = false;

    OperationUpdateOrInsertAppointment(Appointment appointment) {
        this.appointment = appointment;
        if (appointment.getId() <= 0) {
            isInsert = true;
        }
    }

    @Override
    protected Integer execute() throws SQLException, IOException {
        if (isInsert || timeUpdateDoesNotConflict()) {
            String sql = getCommand();
            Connection connection = getConnection();
            PreparedStatement preparedStatement = buildPreparedStatementWithParameters(connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS));
            int rows = preparedStatement.executeUpdate();
            ResultSet resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                return resultSet.getInt(1);
            } else {
                return rows > 0 ? rows : -1;
            }
        }
        return 0;
    }

    private boolean timeUpdateDoesNotConflict() throws IOException, SQLException {
        return JavaSchedulerDatabase.Repository.getAllAppointmentsByDateRangeAndUser(appointment.getStart(), appointment.getEnd(), appointment.getUserId()).isEmpty();
    }

    private String getCommand() {
        return isInsert ? getInsertCommand() : getUpdateCommand();
    }

    private String getInsertCommand() {
        String insertCommand = "INSERT INTO " + AppointmentContract.APPOINTMENT_TABLE_NAME +
                " (" + AppointmentContract.CUSTOMER_ID_COLUMN + ", " +
                AppointmentContract.USER_ID_COLUMN + ", " +
                AppointmentContract.TITLE_COLUMN + ", " +
                AppointmentContract.DESCRIPTION_COLUMN + ", " +
                AppointmentContract.LOCATION_COLUMN + ", " +
                AppointmentContract.CONTACT_COLUMN + ", " +
                AppointmentContract.TYPE_COLUMN + ", " +
                AppointmentContract.URL_COLUMN + ", " +
                AppointmentContract.START_COLUMN + ", " +
                AppointmentContract.END_COLUMN + ", " +
                AppointmentContract.CREATE_BY_COLUMN + ", " +
                AppointmentContract.CREATE_DATE_COLUMN + ", " +
                AppointmentContract.LAST_UPDATE_BY_COLUMN + ", " +
                AppointmentContract.LAST_UPDATE_COLUMN + ") " +
                "SELECT ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? FROM DUAL" +
                " WHERE NOT EXISTS( SELECT 1 FROM " + AppointmentContract.APPOINTMENT_TABLE_NAME +
                " WHERE " + AppointmentContract.USER_ID_COLUMN + " = ? AND (? = " + AppointmentContract.START_COLUMN +
                " OR ? BETWEEN " + AppointmentContract.START_COLUMN + " AND " + AppointmentContract.END_COLUMN +
                " OR ? BETWEEN " + AppointmentContract.START_COLUMN + " AND " + AppointmentContract.END_COLUMN + "));";
        addAppointmentParams();
        addParams(appointment.getUserId());
        addParams(appointment.getStart());
        addParams(appointment.getStart());
        addParams(appointment.getEnd());

        return insertCommand;
    }

    private String getUpdateCommand() {
        String updateCommand = "UPDATE " + AppointmentContract.APPOINTMENT_TABLE_NAME +
                " SET " + AppointmentContract.CUSTOMER_ID_COLUMN + " = ?," +
                AppointmentContract.USER_ID_COLUMN + " = ?," +
                AppointmentContract.TITLE_COLUMN + " = ?," +
                AppointmentContract.DESCRIPTION_COLUMN + " = ?," +
                AppointmentContract.LOCATION_COLUMN + " = ?," +
                AppointmentContract.CONTACT_COLUMN + " = ?," +
                AppointmentContract.TYPE_COLUMN + " = ?," +
                AppointmentContract.URL_COLUMN + " = ?," +
                AppointmentContract.START_COLUMN + " = ?," +
                AppointmentContract.END_COLUMN + " = ?," +
                AppointmentContract.CREATE_BY_COLUMN + " = ?," +
                AppointmentContract.CREATE_DATE_COLUMN + " = ?," +
                AppointmentContract.LAST_UPDATE_BY_COLUMN + " = ?," +
                AppointmentContract.LAST_UPDATE_COLUMN + " = ?" +
                " WHERE " + AppointmentContract.ID_COLUMN + " = ?;";
        addAppointmentParams();
        addParams(appointment.getId());

        return updateCommand;
    }

    private void addAppointmentParams() {
        addParams(appointment.getCustomerId());
        addParams(appointment.getUserId());
        addParams(appointment.getTitle());
        addParams(appointment.getDescription());
        addParams(appointment.getLocation());
        addParams(appointment.getContact());
        addParams(appointment.getType());
        addParams(appointment.getUrl());
        addParams(appointment.getStart());
        addParams(appointment.getEnd());
        addParams(appointment.getCreatedBy());
        addParams(appointment.getCreateDate());
        addParams(appointment.getLastUpdateBy());
        addParams(appointment.getLastUpdate());
    }
}
