package javascheduling.DAL;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javascheduling.DAL.ScheduleContract.UserContract;
import javascheduling.DAL.models.User;

/**
 *
 * @author Michael
 */
class UserMapper extends ResultMapperBase<User> {

    @Override
    protected User Map(ResultSet record) {
        try {
            int id = record.getInt(UserContract.ID_COLUMN);
            boolean active = record.getBoolean(UserContract.ACTIVE_COLUMN);
            LocalDateTime createDate = mapDateFromResultSet(record, UserContract.CREATE_DATE_COLUMN);
            LocalDateTime lastUpdate = mapDateFromResultSet(record, UserContract.LAST_UPDATE_COLUMN);
            String userName = record.getString(UserContract.NAME_COLUMN);
            String password = record.getString(UserContract.PASSWORD_COLUMN);
            String createBy = record.getString(UserContract.CREATED_BY_COLUMN);
            String lastUpdateBy = record.getString(UserContract.LAST_UPDATE_BY_COLUMN);
            User user = new User(id);
            user.setActive(active);
            user.setUserName(userName);
            user.setPassword(password);
            user.setCreatedBy(createBy);
            user.setLastUpdateBy(lastUpdateBy);
            user.setCreateDate(createDate);
            user.setLastUpdate(lastUpdate);
            return user;
        } catch (SQLException ex) {
            Logger.getLogger(UserMapper.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }    
}
