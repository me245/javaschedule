package javascheduling.DAL;

import javascheduling.DAL.ScheduleContract.AddressContract;
import javascheduling.DAL.models.Address;

/**
 *
 * @author michael.evanson
 */
class OperationGetAddressById extends ObjectOperationBase<Address> {

    OperationGetAddressById(int addressId) {
        addParams(addressId);
    }

    @Override
    protected ResultMapperBase<Address> getMapper() {
        return new AddressMapper();
    }

    @Override
    protected String getCommand() {
        return "SELECT " +
                AddressContract.ID_COLUMN + ", " +
                AddressContract.CITY_ID_COLUMN + ", " +
                AddressContract.ADDRESS_COLUMN + ", " +
                AddressContract.ADDRESS2_COLUMN + ", " +
                AddressContract.PHONE_COLUMN + ", " +
                AddressContract.POSTAL_CODE_COLUMN + ", " +
                AddressContract.CREATE_BY_COLUMN + ", " +
                AddressContract.CREATE_DATE_COLUMN + ", " +
                AddressContract.LAST_UPDATE_BY_COLUMN + ", " +
                AddressContract.LAST_UPDATE_COLUMN +
                " FROM " + AddressContract.ADDRESS_TABLE_NAME +
                " WHERE " + AddressContract.ID_COLUMN +
                " = ?;";
    }   
}
