package javascheduling.DAL;

import javascheduling.DAL.ScheduleContract.CountryContract;

/**
 *
 * @author michael.evanson
 */
class OperationIsCountryInDatabase extends BooleanOperationBase {

    OperationIsCountryInDatabase(int countryId) {
        super(CountryContract.COUNTRY_TABLE_NAME, CountryContract.ID_COLUMN);
        addParams(countryId);
    }   
}
