package javascheduling.DAL;

import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;

import javascheduling.DAL.models.Appointment;
import javascheduling.DAL.ScheduleContract.AppointmentContract;

/**
 * @author michael.evanson
 */

public class OperationGetAllAppointmentsByDateRangeAndUser extends ObjectOperationBase<Appointment> {

    OperationGetAllAppointmentsByDateRangeAndUser(LocalDateTime fromDate, LocalDateTime toDate, int userId) {
        addParams(userId);
        addParams(fromDate);
        addParams(toDate);
    }

    @Override
    protected ResultMapperBase<Appointment> getMapper() {
        return new AppointmentMapper();
    }

    @Override
    protected String getCommand() {
        return "SELECT " +
                AppointmentContract.ID_COLUMN + ", " +
                AppointmentContract.CUSTOMER_ID_COLUMN + ", " +
                AppointmentContract.USER_ID_COLUMN + ", " +
                AppointmentContract.TITLE_COLUMN + ", " +
                AppointmentContract.DESCRIPTION_COLUMN + ", " +
                AppointmentContract.LOCATION_COLUMN + ", " +
                AppointmentContract.CONTACT_COLUMN + ", " +
                AppointmentContract.TYPE_COLUMN + ", " +
                AppointmentContract.URL_COLUMN + ", " +
                AppointmentContract.START_COLUMN + ", " +
                AppointmentContract.END_COLUMN + ", " +
                AppointmentContract.CREATE_BY_COLUMN + ", " +
                AppointmentContract.CREATE_DATE_COLUMN + ", " +
                AppointmentContract.LAST_UPDATE_BY_COLUMN + ", " +
                AppointmentContract.LAST_UPDATE_COLUMN +
                " FROM " + AppointmentContract.APPOINTMENT_TABLE_NAME +
                " WHERE " + AppointmentContract.USER_ID_COLUMN +
                " = ? AND " + AppointmentContract.START_COLUMN +
                " BETWEEN ? AND ?;";
    }
    
}
