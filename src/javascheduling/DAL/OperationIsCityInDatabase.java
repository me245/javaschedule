package javascheduling.DAL;

import javascheduling.DAL.ScheduleContract.CityContract;

/**
 *
 * @author michael.evanson
 */
class OperationIsCityInDatabase extends BooleanOperationBase {

    OperationIsCityInDatabase(int cityId) {
        super(CityContract.CITY_TABLE_NAME, CityContract.ID_COLUMN);
        addParams(cityId);
    }   
}
