package javascheduling.DAL;

import javascheduling.DAL.models.Customer;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import javascheduling.DAL.ScheduleContract.CustomerContract;
import javascheduling.DAL.ScheduleContract.AppointmentContract;

public class OperationSoftDeleteCustomer extends OperationBase {

    public OperationSoftDeleteCustomer(int customerId) {
        addParams(customerId);
        addParams(customerId);
    }

    @Override
    protected Integer execute() throws SQLException, IOException {
        String sql = getCommand();
        Connection connection = getConnection();
        PreparedStatement preparedStatement = buildPreparedStatementWithParameters(connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS));
        return preparedStatement.executeUpdate();
    }

    private String getCommand() {
        return "UPDATE " + CustomerContract.CUSTOMER_TABLE_NAME +
                " SET " + CustomerContract.ACTIVE_COLUMN + " = 0" +
                " WHERE " + CustomerContract.ID_COLUMN + " = ?;" +
                " DELETE FROM " + AppointmentContract.APPOINTMENT_TABLE_NAME +
                " WHERE " + AppointmentContract.CUSTOMER_ID_COLUMN + " = ?;";
    }
}
