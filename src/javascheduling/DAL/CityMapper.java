package javascheduling.DAL;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javascheduling.DAL.ScheduleContract.CityContract;
import javascheduling.DAL.models.City;

/**
 *
 * @author michael.evanson
 */
class CityMapper extends ResultMapperBase<City> {

    @Override
    protected City Map(ResultSet record) {
        try {
            int id = record.getInt(CityContract.ID_COLUMN);
            int countryId = record.getInt(CityContract.COUNTRY_ID_COLUMN);
            LocalDateTime createDate = mapDateFromResultSet(record, CityContract.CREATE_DATE_COLUMN);
            LocalDateTime lastUpdate = mapDateFromResultSet(record, CityContract.LAST_UPDATE_COLUMN);
            String cityName = record.getString(CityContract.NAME_COLUMN);
            String createBy = record.getString(CityContract.CREATE_BY_COLUMN);
            String lastUpdateBy = record.getString(CityContract.LAST_UPDATE_BY_COLUMN);
            City city = new City(id);
            city.setCountryId(countryId);
            city.setCity(cityName);
            city.setCreatedBy(createBy);
            city.setLastUpdateBy(lastUpdateBy);
            city.setCreateDate(createDate);
            city.setLastUpdate(lastUpdate);
            return city;
        } catch (SQLException ex) {
            Logger.getLogger(UserMapper.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
}
