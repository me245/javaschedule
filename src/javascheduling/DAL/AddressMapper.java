package javascheduling.DAL;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.logging.Level;
import java.util.logging.Logger;
import javascheduling.DAL.ScheduleContract.AddressContract;
import javascheduling.DAL.models.Address;

/**
 *
 * @author michael.evanson
 */
class AddressMapper extends ResultMapperBase<Address> {

    @Override
    protected Address Map(ResultSet record) {
        try {
            int id = record.getInt(AddressContract.ID_COLUMN);
            int cityId = record.getInt(AddressContract.CITY_ID_COLUMN);
            LocalDateTime createDate = mapDateFromResultSet(record, AddressContract.CREATE_DATE_COLUMN);
            LocalDateTime lastUpdate = mapDateFromResultSet(record, AddressContract.LAST_UPDATE_COLUMN);
            String address1 = record.getString(AddressContract.ADDRESS_COLUMN);
            String address2 = record.getString(AddressContract.ADDRESS2_COLUMN);
            String postalCode = record.getString(AddressContract.POSTAL_CODE_COLUMN);
            String phone = record.getString(AddressContract.PHONE_COLUMN);
            String createBy = record.getString(AddressContract.CREATE_BY_COLUMN);
            String lastUpdateBy = record.getString(AddressContract.LAST_UPDATE_BY_COLUMN);
            Address address = new Address(id);
            address.setCityId(cityId);
            address.setAddress(address1);
            address.setAddress2(address2);
            address.setPostalCode(postalCode);
            address.setPhone(phone);
            address.setCreatedBy(createBy);
            address.setLastUpdateBy(lastUpdateBy);
            address.setCreateDate(createDate);
            address.setLastUpdate(lastUpdate);
            return address;
        } catch (SQLException ex) {
            Logger.getLogger(UserMapper.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
}
