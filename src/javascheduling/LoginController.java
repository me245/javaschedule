/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javascheduling;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Locale;
import java.util.ResourceBundle;

import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javascheduling.DAL.JavaSchedulerDatabase;
import javascheduling.DAL.models.User;

/**
 * @author michael.evanson
 */
public class LoginController implements Initializable {

    private String loginFeedbackMessage;

    @FXML
    private Label localizationLocation;
    @FXML
    private Label localizationLanguage;
    @FXML
    private Label loginFormFeedback;
    @FXML
    private Button loginFormSubmit;
    @FXML
    private TextField loginFormUser;
    @FXML
    private PasswordField loginFormPassword;

    private BooleanBinding userNameValid;

    private BooleanBinding passwordValid;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Locale currentLocale = Locale.getDefault();
        String locationText = rb.getString("locationText");
        String languageText = rb.getString("languageText");
        loginFeedbackMessage = rb.getString("errorMessageText");
        localizationLanguage.setText(languageText + " " + currentLocale.getDisplayCountry());
        localizationLocation.setText(locationText + " " + currentLocale.getDisplayLanguage());
        userNameValid = Bindings.createBooleanBinding(() -> loginFormUser.getText().length() > 0, loginFormUser.textProperty()); // use of lambda to simplify readability on binding
        passwordValid = Bindings.createBooleanBinding(() -> loginFormPassword.getText().length() > 0, loginFormPassword.textProperty()); // use of lambda to simplify readability on binding
        loginFormSubmit.disableProperty().bind(userNameValid.not().or(passwordValid.not()));
        loginFormSubmit.setOnAction(this::handleLogin);
    }

    private void handleLogin(ActionEvent event) {
        String username = loginFormUser.getText();
        String password = loginFormPassword.getText();
        try {
            User user = JavaSchedulerDatabase.Repository.getUserByName(username);
            if (user != null && password.equals(user.getPassword())) {
                launchMain(user);
            } else {
                loginFormFeedback.setText(loginFeedbackMessage);
            }
        } catch (SQLException | IOException e) {
            e.printStackTrace();
            loginFormFeedback.setText(loginFeedbackMessage);
        }
    }

    private void launchMain(User user) {
        Path logFilePath = Paths.get("UserLogFile.txt");
        LocalDateTime localTime = LocalDateTime.now(ZoneId.of("GMT"));
        String logStamp = user.getUserName() + " logged in at: " + localTime.toString() + "\n";
        try{
            Files.size(logFilePath);
        } catch (IOException e) {
            try {
                Files.createFile(logFilePath);
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
        try {
            Files.write(logFilePath, logStamp.getBytes(), StandardOpenOption.APPEND);
        } catch (IOException e) {
            e.printStackTrace();
        }
        javascheduling.JavaScheduling.getInstance().launchMain(user);
    }
}
